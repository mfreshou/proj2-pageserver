import flask    # from flask import was having issues
from flask import Flask,render_template,request

app = Flask(__name__)

@app.route("/<path:subPath>") # Main routing
def index(subPath): #subPath should have the entire path
    if checking(subPath): # Checking to make sure it doesn't contain bad characters
        flask.abort(403) #aborting if it does
    return flask.render_template(subPath) # Sending out the file if .html is specified

@app.errorhandler(404) # 404 Error
def error_404(error):
    return flask.render_template('404.html'), 404 # Calling 404.html and sending the error

@app.errorhandler(403) # 403 Error
def error_403(error):
    return flask.render_template('403.html'), 403 # Calling 403.html and sending the error

@app.errorhandler(Exception) #Any other errors are being counted as 404
def all_exception_handler(error):
    return flask.render_template('404.html'), 404 #sending out 404 errors

def checking(inString): # Checking to see if there are back characters
    """
    If it has any of the bad characters it returns true
    otherwise it is false
    """
    outVari = False
    if inString.find("//") != -1:
        outVari = True
    if inString.find("..") != -1:
        outVari = True
    if inString.find("~") != -1:
        outVari = True
    return outVari


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')


